import ListOrgRepos from './list-org-repos.js'
import { Octokit } from './dependency-octokit.js';

customElements.define('list-org-repos', ListOrgRepos)

export {
	ListOrgRepos,
	Octokit
}
